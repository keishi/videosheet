#!/usr/bin/env node

const cv = require('opencv4nodejs');
const fr = require('face-recognition').withCv(cv);
const stream = require('stream');
const fs = require('fs');
const sharp = require('sharp');
const path = require('path');
const ffmpeg = require('fluent-ffmpeg');
const child_process = require('child_process');

function calculateBlurriness(image) {
  let gx = image.sobel(cv.CV_32F, 1, 0);
  let gy = image.sobel(cv.CV_32F, 0, 1);
  let normGx = gx.norm();
  let normGy = gy.norm();
  let sumSq = normGx * normGx + normGy * normGy;
  return 1 / (sumSq / (image.cols * image.rows) + 1e-6);
}

function calculateSharpness(image) {
  let dst = image.laplacian(cv.CV_64F)
  let o = dst.meanStdDev();
  return o.stddev.at(0, 0) * o.stddev.at(0, 0);
}

class ImagePipe extends stream.Writable {
  constructor(onImageCallback/*(data : Buffer) => void*/) {
    super();
    this.buffer = Buffer.alloc(0);
    this.bmpSize = -1;
    this.onImageCallback = onImageCallback;
    this.index = 0;
  }
  _write(chunk, encoding, callback) {
    console.assert(chunk instanceof Buffer);
    this.buffer = Buffer.concat([this.buffer, chunk]);
    while (true) {
      if (this.bmpSize < 0 && this.buffer.length >= 6) {
        console.assert(this.buffer.slice(0, 2).toString("ascii") === "BM");
        this.bmpSize = this.buffer.readInt32LE(2);
      }
      if (this.bmpSize >= 0 && this.buffer.length >= this.bmpSize) {
        let bmpData = this.buffer.slice(0, this.bmpSize);
        this.onImageCallback(this.index, bmpData);
        this.buffer = this.buffer.slice(this.bmpSize);
        this.bmpSize = -1;
        this.index++;
      } else {
        break;
      }
    }
    callback();
  }
}

function ffprobe(filename) {
  return new Promise((resolve, reject) => {
    ffmpeg(filename)
      .ffprobe((err, data) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(data);
      });
  });
}

function getFirstVideoStream(probe) {
  for (let stream of probe.streams) {
    if (stream['codec_type'] === 'video') {
      return stream;
    }
  }
  return undefined;
}

function getDisplayAspectRatio(stream) {
  let str = stream['display_aspect_ratio'];
  if (str === "N/A") {
    return stream['width'] / stream['height'];
  }
  let match = /(\d+):(\d+)/.exec(str);
  if (match === null) {
    throw `Unexpected display_aspect_ratio: ${str}`;
  }
  let width = parseInt(match[1]);
  let height = parseInt(match[2]);
  if (width === 0 || height === 0) {
    throw `Unexpected display_aspect_ratio: ${str}`;
  }
  return width / height;
}

function getNumKeyframes(filename) {
  return getNumKeyframesFast(filename).catch((error) => {
    console.log(error);
    if (error !== "N/A") {
      throw error;
    }
    return getNumKeyframesSlow(filename);
  })
}

function getNumFrames(filename) {
  return getNumFramesFast(filename).catch((error) => {
    if (error !== "N/A") {
      throw error;
    }
    return getNumFramesSlow(filename);
  })
}

function getNumKeyframesFast(filename) {
  return new Promise((resolve, reject) => {
    child_process.execFile('ffprobe', ["-skip_frame", "nokey", "-count_frames", "-select_streams", "v:0", "-show_entries", "stream=nb_frames", "-of", "json", filename], {maxBuffer: 500 * 1024 * 1024}, (error, stdout, stderr) => {
      if (error) {
        reject(error);
        return;
      }
      let o = JSON.parse(stdout);
      if (!('nb_frames' in o['streams'][0])) {
        return reject('N/A');
      }
      resolve(o['streams'][0]['nb_frames']);
    });
  });
}

function getNumKeyframesSlow(filename) {
  return new Promise((resolve, reject) => {
    // FIXME: Maybe nb_read_frames will work.
    child_process.execFile('ffprobe', ["-skip_frame", "nokey", "-select_streams", "v:0", "-show_entries", "frame=pkt_pts_time", "-of", "json", filename], {maxBuffer: 500 * 1024 * 1024}, (error, stdout, stderr) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(JSON.parse(stdout)['frames'].length);
    });
  });
}

function getNumFramesFast(filename) {
  return new Promise((resolve, reject) => {
    child_process.execFile('ffprobe', ["-count_frames", "-select_streams", "v:0", "-show_entries", "stream=nb_frames", "-of", "json", filename], {maxBuffer: 500 * 1024 * 1024}, (error, stdout, stderr) => {
      if (error) {
        reject(error);
        return;
      }
      let o = JSON.parse(stdout);
      if (!('nb_frames' in o['streams'][0])) {
        return reject('N/A');
      }
      resolve(o['streams'][0]['nb_frames']);
    });
  });
}

function getNumFramesSlow(filename) {
  return new Promise((resolve, reject) => {
    child_process.execFile('ffprobe', ["-count_frames", "-select_streams", "v:0", "-show_entries", "stream=nb_read_frames", "-of", "json", filename], {maxBuffer: 500 * 1024 * 1024}, (error, stdout, stderr) => {
      if (error) {
        reject(error);
        return;
      }
      let o = JSON.parse(stdout);
      if (!('nb_read_frames' in o['streams'][0])) {
        return reject('N/A');
      }
      resolve(o['streams'][0]['nb_read_frames']);
    });
  });
}

// cv.imwrite has a bug where flags are not applied.
function imwrite(filename, image, flags) {
  let data = cv.imencode(path.extname(filename), image, flags);
  fs.writeFileSync(filename, data);
}

async function main(filename, sheetFile, sheetQuality, bestFrameFile, bestFrameQuality) {
  let useKeyframes = true;
  let numFrames = 0;
  try {
    numFrames = await getNumKeyframesSlow(filename); // Fast gives unreliable numbers
  } catch(error) {
    if (error !== "N/A") {
      throw error;
    }
  }
  useKeyframes = numFrames > 64;
  if (!useKeyframes) {
    numFrames = await getNumFrames(filename);
  }
  let numCandidates = Math.max(Math.min(Math.floor(numFrames / 64), 5), 1);
  let captureInterval = (numFrames - numCandidates) / 63;
  console.log("useKeyframes", useKeyframes);
  console.log("numFrames", numFrames);
  console.log("captureInterval", captureInterval);

  const probe = await ffprobe(filename);
  const dar = getDisplayAspectRatio(getFirstVideoStream(probe));
  const scale = dar > 512.0 / 288.0 ? `${Math.ceil(288 * dar).toFixed(0)}:288` : `512:${Math.ceil(512 / dar).toFixed(0)}`;

  let detector = fr.FaceDetector();
  let candidates = [];
  let frames = [];
  let bestFrame = undefined;
  let bestFrameScore = -Infinity;
  let imagePipe = new ImagePipe((i, data) => {
    if (i >= numFrames) {
      return;
    }
    let j = Math.floor(i % captureInterval);
    if (j < numCandidates || numCandidates === 0) {
      let frame = cv.imdecode(data);
      let sharpness = calculateSharpness(frame);
      let frCvImage = new fr.CvImage(frame);
      let faces = detector.locateFaces(frCvImage);
      let faceArea = 0;
      for (let face of faces) {
        faceArea += face.rect.area;
      }
      let faceScore = 1 + Math.log10(faceArea / (512 * 288) + 1) * 10;
      let numFaces = faces.length;
      let sharpnessScore = 1 + Math.log10(sharpness / 200 + 1) * 0.4;
      let score = sharpnessScore * faceScore;
      candidates.push([frame, score]);
      if (score > bestFrameScore) {
        bestFrame = frame;
        bestFrameScore = score;
      }
      if (j === numCandidates - 1 || i === numFrames - 1) {
        candidates.sort((a, b) => { return b[1] - a[1]; });
        frames.push(candidates[0][0]);
        candidates = [];
      }
    }
  });
  imagePipe.on('finish', () => {
    console.log("finish", imagePipe.index);
    if (useKeyframes)
      console.assert(numFrames === imagePipe.index, `numFrames(${numFrames}) did not match imagePipe.index(${imagePipe.index}) useKeyframes ${useKeyframes}`); // getNumFrames() may not be accurate
    let tiled = new cv.Mat(288 * 8, 512 * 8, cv.CV_8UC3, [255, 0, 0]);
    for (let i = 0; i < frames.length; i++) {
      const col = i % 8;
      const row = Math.floor(i / 8);
      const frame = frames[i];
      console.assert(frame.rows === 288, frame.rows);
      console.assert(frame.cols === 512, frame.cols);
      let rect = new cv.Rect(col * 512, row * 288, 512, 288);
      let roi = tiled.getRegion(rect);
      frame.copyTo(roi);
    }
    imwrite(sheetFile, tiled, [cv.IMWRITE_JPEG_QUALITY, sheetQuality, cv.IMWRITE_JPEG_OPTIMIZE, 1]);
    console.log("bestFrameScore", bestFrameScore);
    if (bestFrameFile) {
      imwrite(bestFrameFile, bestFrame, [cv.IMWRITE_JPEG_QUALITY, bestFrameQuality, cv.IMWRITE_JPEG_OPTIMIZE, 1]);
    }
  });

  let ffmpegCmd = ffmpeg(filename);
  if (useKeyframes) {
    ffmpegCmd.addInputOption("-skip_frame", "nokey");
    ffmpegCmd.addOutputOption("-vsync", "0")
  }
  ffmpegCmd.addOutputOption("-vf", `select=eq(pict_type\\,I),scale=${scale},crop=512:288`)
    .format("image2pipe")
    .videoCodec("bmp")
    .pipe(imagePipe);
}


const argv = require('yargs')
  .example('$0 -i <input video> -o <output>', 'generate video thumbnail')
  .option('input', {
    alias: 'i',
    demandOption: true,
    describe: 'input video file',
    type: 'string'
  })
  .option('output', {
    alias: 'o',
    demandOption: true,
    describe: 'output image file',
    type: 'string'
  })
  .option('best', {
    alias: 'b',
    demandOption: false,
    describe: 'output best frame file',
    type: 'string'
  })
  .argv;

main(argv.input, argv.output, 30, argv.best, 80);
